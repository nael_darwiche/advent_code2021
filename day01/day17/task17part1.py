class Solution:
    def __init__(self):
        self.target_area_x = (60, 94)
        self.target_area_y = (-171, -136)
        self.initial_x_velocities = []
        self.initial_y_velocity = 0

    def find_initial_x_velocities(self):  # Case where target area is reached at a velocity x of 0
        for x in range(self.target_area_x[0], self.target_area_x[1] + 1):
            n = (-1 + (1 + 8 * x) ** (1 / 2)) / 2
            if int(n) == n:
                self.initial_x_velocities.append(int(n))
            else:
                continue

    def target_area_is_reached(self, y_0: int) -> bool:  # Case where initial_y_velocity(y_0) > initial_x_velocity
        i = 0
        while True:
            i += 1
            n = i * (- y_0 - 1) - (i * (i - 1)) / 2
            if self.target_area_y[0] <= n <= self.target_area_y[1]:
                return True
            elif n < self.target_area_y[0]:
                return False
            else:
                continue

    def main(self):
        self.find_initial_x_velocities()
        x_0 = self.initial_x_velocities[0]
        for y_0 in range(x_0, -self.target_area_y[0] + 1):
            if self.target_area_is_reached(y_0):
                self.initial_y_velocity = y_0
        return (self.initial_y_velocity * (self.initial_y_velocity + 1)) / 2


if __name__ == '__main__':
    sol = Solution()
    print(sol.main())
