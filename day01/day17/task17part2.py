from collections import defaultdict


class Solution:
    def __init__(self):
        self.target_area_x = (60, 94)
        self.target_area_y = (-171, -136)
        self.x_0_steps_dic = defaultdict(list)
        self.y_0_steps_dic = defaultdict(list)
        self.len_y_0_steps_dic = 0

    def classify_x_0_velocity(self, x_0: int):  # We classify x_0 in self.x_0_steps_dic
        i = 0
        while i * x_0 - int(((i - 1) * i) / 2) <= self.target_area_x[1]:
            if i * x_0 - int(((i - 1) * i) / 2) == (i + 1) * x_0 - int(((i + 1) * i) / 2):
                if self.target_area_x[0] <= i * x_0 - int(((i - 1) * i) / 2) <= self.target_area_x[1]:
                    self.deal_with_infinity_x_0(i, x_0)
                    break
                else:
                    break
            i += 1
            n = i * x_0 - int(((i - 1) * i) / 2)
            if self.target_area_x[0] <= n <= self.target_area_x[1]:
                self.x_0_steps_dic[i].append(x_0)

    def deal_with_infinity_x_0(self, i: int, x_0: int):
        for m in range(i, self.len_y_0_steps_dic + 1):
            self.x_0_steps_dic[m].append(x_0)

    def classify_y_0_velocity_upward(self, y_0: int):  # We classify y_0 in self.y_0_steps_dic and y_0 > 0
        i = 0
        while i * (-y_0 - 1) - int(((i - 1) * i) / 2) >= self.target_area_y[0]:
            i += 1
            n = i * (-y_0 - 1) - int(((i - 1) * i) / 2)
            if self.target_area_y[0] <= n <= self.target_area_y[1]:
                self.y_0_steps_dic[i + 2 * y_0 + 1].append(y_0)

    def classify_y_0_velocity_downward(self, y_0: int):  # We classify y_0 in self.y_0_steps_dic and y_0 <= 0
        i = 0
        while i * y_0 - int(((i - 1) * i) / 2) >= self.target_area_y[0]:
            i += 1
            n = i * y_0 - int(((i - 1) * i) / 2)
            if self.target_area_y[0] <= n <= self.target_area_y[1]:
                self.y_0_steps_dic[i].append(y_0)

    def build_dictionaries(self):
        for y_0 in range(1, - self.target_area_y[0] + 1):
            self.classify_y_0_velocity_upward(y_0)
        for y_0 in range(- self.target_area_y[0] + 1):
            self.classify_y_0_velocity_downward(- y_0)
        self.len_y_0_steps_dic = max(self.y_0_steps_dic.keys())
        for x_0 in range(1, self.target_area_x[1] + 1):
            self.classify_x_0_velocity(x_0)

    def count(self):
        ans = []
        self.build_dictionaries()
        for n in self.x_0_steps_dic.keys():
            for x_0 in set(self.x_0_steps_dic[n]):
                for y_0 in set(self.y_0_steps_dic[n]):
                    ans.append((x_0, y_0))
        return len(set(ans))


if __name__ == '__main__':
    sol = Solution()
    print(sol.count())
