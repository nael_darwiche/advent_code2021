def after_one_day(*fish: str):  # QUESTION
    counter = 0
    for i, num in enumerate(map(int, fish)):
        if num > 0:
            list_fish[i] = int(list_fish[i]) - 1
        else:
            list_fish[i] = 6
            counter += 1
    list_fish.extend([8]*counter)
    return list_fish


def main(input_file, days):
    with open(input_file) as f:
        global list_fish
        list_fish = f.readlines()[0].split(',')
        for i in range(days):
            list_fish = after_one_day(*list_fish)
            print(i)
    print(f'There are {len(list_fish)} lantern fish after {days} days.')


if __name__ == '__main__':
    main('input_task06.txt', 256)
