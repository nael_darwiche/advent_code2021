def after_one_day(*fish: str):  #QUESTION
    counter = 0
    list_fish = list(fish)  # fish is a tuple
    for i, num in enumerate(map(int, fish)):
        if num > 0:
            list_fish[i] = int(list_fish[i])-1
        else:
            list_fish[i] = 6
            counter += 1
    for _ in range(counter):
        list_fish.append('8')
    return list_fish


def main(input_file, days):
    with open(input_file) as f:
        f = f.readlines()[0].split(',')
        for i in range(days):
            f = after_one_day(*f)
            print(i)
    print(f'There are {len(f)} lantern fish after {days} days.')


if __name__ == '__main__':
    main('input_task06.txt', 80)
