def after_one_day(fish_energy: dict):
    # time passes... reduce timer for all groups of fishes by 1
    for timer_value in range(9):
        fish_energy[timer_value - 1] = fish_energy[timer_value]

    # Special dealing with fishes with timer at -1
    fish_energy[6] += fish_energy[-1]
    fish_energy[8] = fish_energy[-1]
    del fish_energy[-1]


def read_energies_from_file(filepath: str) -> dict:
    # init_dict
    energies = dict.fromkeys(range(9), 0)
    with open(filepath) as f:
        lis = map(int, f.read().split(','))
        print(lis)
    for num in lis:
        energies[num] += 1
    return energies


if __name__ == '__main__':
    days = 256
    energies = read_energies_from_file("input_task06.txt")
    for _ in range(days):
        after_one_day(energies)
    n_fishes = sum(energies.values())
    print(n_fishes)
