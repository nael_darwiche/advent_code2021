command = []
mostcommon = ['0b']
lesscommon = ['0b']

with open('input_task03.txt') as f:
    for num in f:
        command.append(num)
    length = len(command)

    for bit in range(len(command[0])):
        zeros = 0

        for num in command:

            if num[bit] == '0':
                zeros += 1

        if zeros < length / 2:
            mostcommon.append('1')
            lesscommon.append('0')
        else:
            mostcommon.append('0')
            lesscommon.append('1')

lesscommonansw = ''.join(lesscommon)
mostcommonansw = ''.join(mostcommon)

print(lesscommonansw)
print(mostcommonansw)
print(1519 * 2576)
