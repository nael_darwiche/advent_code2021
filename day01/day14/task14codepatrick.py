from collections import Counter
from typing import Tuple, List


class Task14:
    def __init__(self):
        self.polymer_template = ""
        self.pair_insertions = {}

    def read_file(self, filepath: str) -> None:
        with open(filepath, "r") as f:
            self.polymer_template = next(f).strip(" \n")
            next(f)
            for line in f:
                self.pair_insertions.update(self.parse_instruction(line.strip(" \n")))

    @staticmethod
    def parse_instruction(line: str) -> dict:
        k, v = line.split(" -> ")
        return {k: v}

    def step(self, n_steps=1):
        for _ in range(n_steps):
            new_template = ""
            for idx in range(len(self.polymer_template) - 1):
                comb = self.polymer_template[idx:idx + 2]
                new_template += comb[0]
                new_template += self.pair_insertions.get(comb, "")
            new_template += self.polymer_template[-1]
            self.polymer_template = new_template

    def n_most_common_least_common(self):
        c = Counter(self.polymer_template)
        return max(c.values()), min(c.values())

    def result(self):
        c = Counter(self.polymer_template)
        return max(c.values()) - min(c.values())