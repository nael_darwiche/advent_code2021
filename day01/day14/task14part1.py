def next_step(list_elements: list[str], dic_pair_rules: dict) -> list[str]:
    num_of_insertions = len(list_elements) - 1
    for i in range(1, 2 * num_of_insertions, 2):
        list_elements.insert(i, dic_pair_rules[''.join(list_elements[i-1:i + 1])])

    return list_elements


def main(input_file, steps: int):
    with open(input_file) as f:
        save = f.readlines()
        starting_polymer = list(save[0].strip('\n'))
        dic_pair_rules = {}
        counters_list = []
        for line in save[2:]:
            pair, result = line.strip('\n').split(' -> ')
            dic_pair_rules[pair] = result
        for i in range(steps):
            starting_polymer = next_step(starting_polymer, dic_pair_rules)
            #print(starting_polymer)
            print(i)
        for letter in set(starting_polymer):
            counters_list.append(starting_polymer.count(letter))

    return sorted(counters_list)


if __name__ == '__main__':
    print(main('test.txt', 10))
