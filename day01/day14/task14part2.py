from collections import defaultdict


class Polymer:
    def __init__(self):
        self.dic = {}
        self.first = ""
        self.last = ""
        self.first_line = ""

    def read_file(self, input_file) -> dict:
        with open(input_file) as f:
            lines = f.readlines()
            self.first_line = lines[0]
            self.first = lines[0][0]
            self.last = lines[0][-1]
            for line in lines[2:]:
                self.dic[line.strip('\n').split(' -> ')[0]] = line.strip('\n').split(' -> ')[1]
        return self.dic

    def return_pairs_of_polymers(self, pair: str) -> (str, str):
        to_add = self.dic[pair]
        return pair[0] + to_add, to_add + pair[1]

    def initialize_pairs_dict(self) -> dict:
        init_dict = defaultdict(int)
        init_line = self.first_line
        for i in range(len(init_line) - 1):
            init_dict[init_line[i] + init_line[i + 1]] += 1
        return init_dict

    def after_one_step(self, state_of_pairs: dict) -> dict:  # state_of_pairs cannot be changed #Can it be changed ??#
        new_template_dic = defaultdict(int)
        constant = {}
        constant = state_of_pairs.copy()
        for pair in self.dic:
            num = constant[pair]
            i, j = self.return_pairs_of_polymers(pair)
            new_template_dic[i] += num
            new_template_dic[j] += num
        return new_template_dic

    def get_number_of_final_pairs(self, days: int):
        polymer_template = self.initialize_pairs_dict()
        for _ in range(days):
            polymer_template = self.after_one_step(polymer_template)
        return polymer_template

    def get_num_of_letters(self, state_of_pairs: dict) -> dict:
        occurences_of_letters_dic = defaultdict(int)
        for k, v in state_of_pairs.items():
            for letter in k:
                occurences_of_letters_dic[letter] += v
        occurences_of_letters_dic[self.first] += 1
        occurences_of_letters_dic[self.last] += 1
        return {letter: occ / 2 for letter, occ in occurences_of_letters_dic.items()}

    def main(self, input_file, days):
        self.read_file(input_file)
        state_of_pairs = self.get_number_of_final_pairs(days)
        letter_values = self.get_num_of_letters(state_of_pairs)
        return max(letter_values.values()) - min(letter_values.values())


if __name__ == '__main__':
    pol = Polymer()
    print(pol.main('input.txt', 40))
