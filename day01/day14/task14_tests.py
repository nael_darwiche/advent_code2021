import unittest
import task14


class TestTask14(unittest.TestCase):
    def setUp(self) -> None:
        self.polymer = task14

    def test_read_input(self):
        self.polymer.read_file("task14_input_example.txt")
        self.assertEqual(self.polymer.polymer_template, "NNCB")
        self.assertEqual(self.polymer.pair_insertions, {'BB': 'N',
                                                        'BC': 'B',
                                                        'BH': 'H',
                                                        'BN': 'B',
                                                        'CB': 'H',
                                                        'CC': 'N',
                                                        'CH': 'B',
                                                        'CN': 'C',
                                                        'HB': 'C',
                                                        'HC': 'B',
                                                        'HH': 'N',
                                                        'HN': 'C',
                                                        'NB': 'B',
                                                        'NC': 'B',
                                                        'NH': 'C',
                                                        'NN': 'C'})

    def test_step_simple(self):
        self.polymer.polymer_template = "NNCB"
        self.polymer.pair_insertions = {"NN": "B"}
        self.polymer.step()
        self.assertEqual(self.polymer.polymer_template, "NBNCB")

    def test_step_complex(self):
        self.polymer.polymer_template = "NNCB"
        self.polymer.pair_insertions = {"NN": "B", "NC": "N", "CB": "A"}
        self.polymer.step()
        self.assertEqual(self.polymer.polymer_template, "NBNNCAB")

    def test_n_most_common_least_common(self):
        self.polymer.polymer_template = "NNCB"
        self.assertEqual(self.polymer.n_most_common_least_common(), (2, 1))

    def test_full_example(self):
        self.polymer.read_file("task14_input_example.txt")
        self.polymer.step(10)
        self.assertEqual(self.polymer.n_most_common_least_common(), (1749, 161))
        self.assertEqual(self.polymer.result(), 1588)

    def test_full(self):
        self.polymer.read_file("task14_input.txt")
        self.polymer.step(10)
        self.assertEqual(self.polymer.n_most_common_least_common(), (3098, 514))
        self.assertEqual(self.polymer.result(), 2584)

if __name__ == '__main__':