last = None

with open('input.txt') as f:
    counter = 0
    last = (next(f), next(f), next(f))
    for line in f:
        current = (last[1], last[2], line)
        if sum(map(int, current)) > sum(map(int, last)):
            counter += 1
        print(f'last: {last}, current: {current}, counter: {counter}')
        last = current
print(f'Final Count: {counter}')
