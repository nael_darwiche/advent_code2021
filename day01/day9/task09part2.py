from typing import List


def check_if_low_point(num: int, x: int, y: int, lines: list) -> bool:
    value = False
    if 0 < y < len(lines) - 1 and 0 < x < len(lines[0]) - 1:
        to_check = (int(lines[y - 1][x]), int(lines[y][x - 1]), int(lines[y][x + 1]), int(lines[y + 1][x]), num)
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    elif x == y:
        if x == 0 and y == 0:
            to_check = (num, int(lines[0][1]), int(lines[1][0]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
    elif x == len(lines) and y == 0:
        to_check = (int(lines[0][98]), num, int(lines[1][99]))
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    elif x == 99 and y == len(lines) - 1:
        to_check = (int(lines[len(lines) - 2][99]), num, int(lines[len(lines) - 1][98]))
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    elif x == 0 and y == len(lines) - 1:
        to_check = (int(lines[len(lines) - 2][0]), num, int(lines[len(lines) - 1][1]))
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    else:
        if x == 0:
            to_check = (int(lines[y - 1][0]), num, int(lines[y][1]), int(lines[y + 1][0]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
        elif x == 99:
            to_check = (int(lines[y - 1][99]), num, int(lines[y][98]), int(lines[y + 1][99]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
        elif y == 0:
            to_check = (int(lines[0][x - 1]), num, int(lines[1][x]), int(lines[0][x + 1]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
        elif y == len(lines) - 1:
            to_check = (
                int(lines[len(lines) - 1][x - 1]), num, int(lines[len(lines) - 2][x]),
                int(lines[len(lines) - 1][x + 1]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
    return value


def get_low_points(input_file):
    with open(input_file) as f:
        low_points = []
        heightmaps = [line.strip('\n') for line in f.readlines()]
        for cord_x in range(len(heightmaps[0])):
            for cord_y in range(len(heightmaps)):
                number = int(heightmaps[cord_y][cord_x])
                if check_if_low_point(number, cord_x, cord_y, heightmaps):
                    low_points.append((cord_x, cord_y))
    return low_points


def count_size_of_basin(input_file, low_point: (int, int)) -> int:
    with open(input_file) as f:
        heightmaps = [line.strip('\n') for line in f.readlines()]
    counter = 1
    already_checked_places = [low_point]
    list_of_coord_to_check = [low_point]
    while list_of_coord_to_check:
        x, y = list_of_coord_to_check.pop()
        #elements_to_delete = len(list_of_coord_to_check)
        #print(elements_to_delete, number)
        neighbors = [
                (x - 1, y),
                (x, y - 1),
                (x + 1, y),
                (x, y + 1)
            ]
        for x_to_check, y_to_check in neighbors:
            if not 0 <= y_to_check < len(heightmaps):
                continue
            elif not 0 <= x_to_check < len(heightmaps[y_to_check]):
                continue
            elif (x_to_check, y_to_check) in already_checked_places:
                continue
            elif int(heightmaps[y_to_check][x_to_check]) != 9:
                already_checked_places.append((x_to_check, y_to_check))
                list_of_coord_to_check.append((x_to_check, y_to_check))
                #print(heightmaps[y_to_check][x_to_check], type(heightmaps[y_to_check][x_to_check]))
                #print(type(x_to_check), type(y_to_check))
                counter += 1
            else:
                continue
    return counter


def main(input_file):
    size_of_basins = []
    for i, low_point in enumerate(get_low_points(input_file)):
        print(count_size_of_basin(input_file, low_point))
        size_of_basins.append(count_size_of_basin(input_file, low_point)) # !!!!! doesn't append
        size_of_basins.sort()

    return size_of_basins


if __name__ == '__main__':
    print(main('input_task09.txt'))
    print(95*99*101)



