def check_if_low_point(num: int, x: int, y: int, lines: list) -> bool:
    value = False
    if 0 < y < len(lines) - 1 and 0 < x < len(lines[0]) - 1:
        to_check = (int(lines[y - 1][x]), int(lines[y][x - 1]), int(lines[y][x + 1]), int(lines[y + 1][x]), num)
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    elif x == y:
        if x == 0 and y == 0:
            to_check = (num, int(lines[0][1]), int(lines[1][0]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
    elif x == 99 and y == 0:
        to_check = (int(lines[0][98]), num, int(lines[1][99]))
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    elif x == 99 and y == len(lines) - 1:
        to_check = (int(lines[len(lines) - 2][99]), num, int(lines[len(lines) - 1][98]))
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    elif x == 0 and y == len(lines) - 1:
        to_check = (int(lines[len(lines) - 2][0]), num, int(lines[len(lines) - 1][1]))
        if min(to_check) == num and to_check.count(num) == 1:
            value = True
    else:
        if x == 0:
            to_check = (int(lines[y - 1][0]), num, int(lines[y][1]), int(lines[y + 1][0]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
        elif x == 99:
            to_check = (int(lines[y - 1][99]), num, int(lines[y][98]), int(lines[y + 1][99]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
        elif y == 0:
            to_check = (int(lines[0][x - 1]), num, int(lines[1][x]), int(lines[0][x + 1]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
        elif y == len(lines) - 1:
            to_check = (
                int(lines[len(lines) - 1][x - 1]), num, int(lines[len(lines) - 2][x]),
                int(lines[len(lines) - 1][x + 1]))
            if min(to_check) == num and to_check.count(num) == 1:
                value = True
    return value


def get_low_points(input_file):
    with open(input_file) as f:
        low_points = []
        heightmaps = [line.strip('\n') for line in f.readlines()]
        for cord_x in range(len(heightmaps[0])):
            for cord_y in range(len(heightmaps)):
                number = int(heightmaps[cord_y][cord_x])
                if check_if_low_point(number, cord_x, cord_y, heightmaps):
                    low_points.append((cord_x, cord_y))
    return low_points