import task
import unittest


class TestTask13Origami(unittest.TestCase):
    def setUp(self):
        self.origami = task.Task13Origami()

    def test_read_file(self):
        """Require that test file can be read, and we have a number of points and instructions"""
        self.origami.read_file("test.txt")
        self.assertEqual(len(self.origami.instructions), 2)
        self.assertEqual(len(self.origami.points), 18)

    def test_parse_point(self):
        point = task.Task13Origami.parse_point('6,12')
        self.assertEqual(point, (6, 12))

    def test_parse_instruction(self):
        instruction1 = task.Task13Origami.parse_instruction("fold along y=7")
        self.assertEqual(instruction1, (task.AXIS.y, 7))
        instruction2 = task.Task13Origami.parse_instruction("fold along x=5")
        self.assertEqual(instruction2, (task.AXIS.x, 5))

    def test_fold_1_point_along_x(self):
        """Test Folding along x axis"""
        self.origami.points = [(1, 1)]
        self.origami.fold_along_x(6)
        self.assertEqual(self.origami.points, [(11, 1)])

    def test_fold_1_point_along_x_overlap(self):
            """Test Folding along x axis overlap"""
            self.origami.points = [(11, 5), (1, 5)]
            self.origami.fold(task.AXIS.x, 6)
            self.assertEqual(self.origami.points, [(1, 5)])

    def test_fold_1_point_along_y(self):
            """Test Folding along y axis"""
            self.origami.points = [(5, 11)]
            self.origami.fold(task.AXIS.y, 6)
            self.assertEqual(self.origami.points, [(5, 1)])

    def test_fold_1_point_along_y_overlap(self):
            """Test Folding along y axis with overlap"""
            self.origami.points = [(5, 11), (5, 1)]
            self.origami.fold(task.AXIS.y, 6)
            self.assertEqual(self.origami.points, [(5, 1)])

    def test_fold_all_points(self):
            """Test Folding all file twice"""
            self.origami.read_file("input_task13_example.txt")
            axis_x = task.AXIS.x
            axis_y = task.AXIS.y
            self.origami.fold(axis_y, 7)
            self.assertEqual(len(self.origami.points), 17)
            self.origami.fold(axis_x, 5)
            self.assertEqual(len(self.origami.points), 16)

if __name__ == '__main__':
    unittest.main()
