f = open('input_task05.txt')
lines = f.readlines()
dict = {}
counter = 1
new_dict = {}



#print(lines)
lstx1 = []
lstx2 = []
lsty1 = []
lsty2 = []
for line in lines:

    pt1 = line.split()[0]
    pt2 = line.split()[2]

    x1 = pt1.split(',')[0]
    lstx1.append(x1)
    y1 = pt1.split(',')[1]
    lsty1.append(y1)
    x2 = pt2.split(',')[0]
    lstx2.append(x2)
    y2 = pt2.split(',')[1]
    lsty2.append(y2)


vert_lines = [('x', i, j,  k) for i, j, h, k in zip(lstx1, lsty1, lstx2, lsty2) if i == h and j != k]
print(vert_lines)
hor_lines = [('y', j, i, h)for i, j, h, k in zip(lstx1, lsty1, lstx2, lsty2) if j == k and i != h]
print(hor_lines)

for vert_line in vert_lines:
    for hor_line in hor_lines:
        #print(vert_line, hor_line)
        if min(vert_line[2], vert_line[3]) < hor_line[1] < max(vert_line[2], vert_line[3]) and min(hor_line[2], hor_line[3]) < vert_line[1] < max(hor_line[2], hor_line[3]):
            if (vert_line[1], hor_line[1]) in dict:
                dict[vert_line[1], hor_line[1]] = counter+1  #no need to add for more than 2

            else:
                dict[vert_line[1], hor_line[1]] = counter

answers = [answer for answer in dict.values() if answer >= 2]
print(len(answers))
print(dict)

for i, hor_line1 in enumerate(hor_lines):
    for hor_line2 in hor_lines[i+1:]:
        #print(hor_line1, hor_line2)
        min_h1 = min(hor_line1[2], hor_line1[3])
        max_h1 = max(hor_line1[2], hor_line1[3])
        min_h2 = min(hor_line2[2], hor_line2[3])
        max_h2 = max(hor_line2[2], hor_line2[3])
        x = max(min_h1, min_h2)
        y = min(max_h1, max_h2)
        if hor_line1[1] == hor_line2[1] and x < y:
            for i in range(int(x), int(y)+1):
                if (hor_line1, i) in dict:
                    dict[hor_line1, i] = counter
                else:
                    dict[hor_line1, i] = counter + 1 # no need to go higher than 2

for vert_line1 in vert_lines:
    x2 = min(vert_line1[2], vert_line1[3])
    y2 = max(vert_line1[2], vert_line1[3])
    if vert_line1[1] in new_dict and max(new_dict[vert_line1[1]][0], x2) < min(new_dict[vert_line1[1]][1], y2):  #verifies if there is a same 'x' line AND if max(x1, x2) < min(y1, y2). In other words, if the lines touch themselves
            x = max(new_dict[vert_line1[1]][0], x2)
            y = min(new_dict[vert_line1[1]][1], y2)
            print(x, y)  # the points were the lines are superposed
            print(vert_line1, (new_dict[vert_line1[1]][0], new_dict[vert_line1[1]][1]))  # test to compare both vertical lines
            for i in range(int(x), int(y)+1):
                if (vert_line1[1], i) in dict:
                    dict[vert_line1[1], i] = counter
                else:
                    dict[vert_line1[1], i] = counter + 1 #no need to go higher than 2
    else:
        new_dict[vert_line1[1]] = (min(x2, y2), max(x2, y2))  # adds the 'x' line to new_dict and assign it to its terminals(bornes).

answers = [answer for answer in dict.values() if answer >= 2]
print(len(answers), 'l')




