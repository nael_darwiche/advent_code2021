from collections import defaultdict


def line_to_coords(line: str) -> ((int, int), (int, int)):
    coord1, coord2 = line.split('->')
    coord1 = tuple(map(int, coord1.split(',')))
    coord2 = tuple(map(int, coord2.split(',')))
    return coord1, coord2


def fill_hor_line(coord1: (int, int), coord2: (int, int), steam_at_coordinate: dict):
    if coord1[0] > coord2[0]:
        coord1, coord2 = coord2, coord1
    for i in range(coord1[0], coord2[0] + 1):
        steam_at_coordinate[(i, coord1[1])] += 1  # add default dict


def fill_vert_line(coord1: (int, int), coord2: (int, int), steam_at_coordinate: dict):
    if coord1[1] > coord2[1]:
        coord1, coord2 = coord2, coord1
    for i in range(coord1[1], coord2[1] + 1):
        steam_at_coordinate[(coord1[0], i)] += 1  # add default dict


def fill_diagonal_line_pos(coord1: (int, int), coord2: (int, int), steam_at_coordinate: dict):
    if coord1[1] > coord2[1]:
        coord1, coord2 = coord2, coord1
    for i in range(coord2[0] - coord1[0] + 1):
        steam_at_coordinate[(coord1[0] + i, coord1[1] + i)] += 1


def fill_diagonal_line_neg(coord1: (int, int), coord2: (int, int), steam_at_coordinate: dict):
    if coord1[1] > coord2[1]:
        coord1, coord2 = coord2, coord1
    for i in range(abs(coord2[0] - coord1[0]) + 1):
        steam_at_coordinate[(coord1[0] - i, coord1[1] + i)] += 1


def main(input_file):
    steam_at_coordinate = defaultdict(int)
    with open(input_file, 'r') as f:
        for line in f:
            coord1, coord2 = line_to_coords(line)
            if coord1[0] == coord2[0]:
                fill_vert_line(coord1, coord2, steam_at_coordinate)
            elif coord1[1] == coord2[1]:
                fill_hor_line(coord1, coord2, steam_at_coordinate)
            elif (coord2[1]-coord1[1])/(coord2[0]-coord1[0]) == -1:
                fill_diagonal_line_neg(coord1, coord2, steam_at_coordinate)
            elif (coord2[1]-coord1[1])/(coord2[0]-coord1[0]) == 1:
                fill_diagonal_line_pos(coord1, coord2, steam_at_coordinate)
    counter = 0
    for x in steam_at_coordinate.values():
        if x > 1:
            counter += 1
    print(f'Found {counter} overlaps in {input_file}')


if __name__ == '__main__':
    main('input_task05.txt')

