from collections import defaultdict

with open('input_task12.txt') as f:
    lis = []
    dict_of_neighbors = defaultdict(list)
    for line in f.readlines():
        lis.append(line.strip('\n').split('-'))
    for couple in lis:
        dict_of_neighbors[couple[0]].append(couple[1])
        dict_of_neighbors[couple[1]].append(couple[0])
    dict_of_neighbors['end'] = []
    print(dict_of_neighbors)


def create_valid_neighbors(current_and_past_steps: list[str]):
    all_valid_neighbors = []
    for to_check in dict_of_neighbors[current_and_past_steps[0][0]]:
        if to_check == 'start':
            continue
        if to_check in current_and_past_steps[1:] and to_check.islower() and current_and_past_steps[0][1] == 1:
            continue
        else:
            to_check_list = []
            if current_and_past_steps[0][1] == 1:
                current_and_past_steps[0] = current_and_past_steps[0][0]
                to_check_list = current_and_past_steps
                to_check_list.insert(0, [to_check, 1])
                all_valid_neighbors.append(to_check_list)
            else:
                current_and_past_steps[0] = current_and_past_steps[0][0]
                to_check_list = current_and_past_steps
                if to_check in to_check_list:
                    to_check_list.insert(0, [to_check, 1])
                    all_valid_neighbors.append(to_check_list)
                else:
                    to_check_list.insert(0, [to_check, 0])
                    all_valid_neighbors.append(to_check_list)
            print(all_valid_neighbors)

    return all_valid_neighbors




def main(input_file):
    counter = 0
    all_trees_list = [[['start', 0]]]
    while all_trees_list:
        paths_list = []
        for i, lis_current_and_past in enumerate(all_trees_list):
            del all_trees_list[i]
            for path in create_valid_neighbors(lis_current_and_past):
                if path[0][0] == 'end':
                    counter += 1
                    continue
                paths_list.append(path)
                print(path, 'path')
        for j in paths_list:
            all_trees_list.append(j)
    return counter


if __name__ == '__main__':
    print(main('input_task12.txt'))

