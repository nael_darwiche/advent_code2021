class Solution:
    def __init__(self):
        self.deepness = 0
        self.next_str = 0
        self.version_nums = 0
        self.packet_list = []
        self.id_zero_locations = {}

    def deal_with_lit_val_in_id_zero_locations(self, val: bool) -> int:  #If true, then it is type id_0, i.e. [[1], x] to DELETE, if False, it is a literal value, i.e. [1]
        if val:
            to_check = self.deepness - 1
        else:
            to_check = self.deepness
        while self.return_length_of_list(self.packet_list, to_check - 1) == 1 and to_check - 1 >= 0:
            to_check -= 1
        if to_check - 1 < 0:
            self.packet_list = []
        elif self.return_length_of_list(self.packet_list, to_check - 1) != 1:
            if to_check - 1 in self.id_zero_locations:
                self.modify_list(self.packet_list, to_check, [])
                self.deepness = to_check  #self.deepness-1 = "initial to_check"
            else:
                self.del_elem_from_list(self.packet_list, to_check)
                self.deepness = to_check

    @staticmethod
    def modify_list(values: list, depth: int, new_val: list):
        for _ in range(depth - 1):
            values = values[0]
        values[0] = new_val

    @staticmethod
    def del_elem_from_list(values: list, depth: int):
        for _ in range(depth - 1):
            values = values[0]
        values.pop(0)

    @staticmethod
    def return_length_of_list(values: list, depth: int) -> int:
        for _ in range(depth):
            values = values[0]
        return len(values)

    @staticmethod
    def get_binary(hexadecimal: str) -> str:
        return format(eval('0x' + hexadecimal), "#0{}b".format(len(hexadecimal) * 4 + 2))[2:]

    @staticmethod
    def len_lit_val_and_extra_zeros(packet: str, start: int) -> int:
        next_char = start + 6
        while int(packet[next_char]) == 1:
            next_char += 5
        return next_char + 4 + 1 - start

    def is_operator_id_0(self, packet: str) -> bool:
        return int(packet[self.next_str + 2 + 1: self.next_str + 5 + 1], 2) != 4 and int(packet[self.next_str + 6]) == 0

    def is_operator_id_1(self, packet: str) -> bool:
        return int(packet[self.next_str + 2 + 1: self.next_str + 5 + 1], 2) != 4 and int(packet[self.next_str + 6]) == 1

    def deal_with_id_zero_bits(self, jump: int):
        for key in self.id_zero_locations.copy():
            self.id_zero_locations[key] -= jump  # The id zero bits number stays the same within the packet list !
            if self.id_zero_locations[key] == 0:
                del self.id_zero_locations[key]
                if key == 0:
                    self.packet_list = []
                else:
                    self.deal_with_lit_val_in_id_zero_locations(True)

    def deal_with_operator_id_0(self, packet: str):
        self.version_nums += int(packet[self.next_str:self.next_str + 2 + 1], 2)
        len_in_bits = int(packet[self.next_str + 7:self.next_str + 7 + 15], 2)
        if self.deepness:
            self.modify_list(self.packet_list, self.deepness, [[1], len_in_bits])
        else:
            self.packet_list = [[1], len_in_bits]
        self.deepness += 1
        self.next_str += 22

        self.deal_with_id_zero_bits(22)  # Deal with id zero bits num before appending the current one !

        self.id_zero_locations[self.deepness - 1] = len_in_bits
        self.id_zero_locations = dict(sorted(self.id_zero_locations.items(), key=None, reverse=True))

    def deal_with_operator_id_1(self, packet: str):
        self.version_nums += int(packet[self.next_str:self.next_str + 2 + 1], 2)
        hor_len = int(packet[self.next_str + 7: self.next_str + 7 + 11], 2)
        list_to_insert = []
        for _ in range(hor_len):
            list_to_insert.append([1])
        if self.deepness:
            self.modify_list(self.packet_list, self.deepness, list_to_insert)
        else:
            self.packet_list = list_to_insert
        self.deepness += 1
        self.next_str += 18

        self.deal_with_id_zero_bits(18)

    def deal_with_literal_values(self, packet: str):
        length = self.len_lit_val_and_extra_zeros(packet, self.next_str)  # Must code the function !!!
        self.version_nums += int(packet[self.next_str:self.next_str + 2 + 1], 2)
        self.next_str += length
        if self.deepness - 1 in self.id_zero_locations:
            self.modify_list(self.packet_list, self.deepness, [])
        else:  # Delete literal value
            self.deal_with_lit_val_in_id_zero_locations(False)

        self.deal_with_id_zero_bits(length)

    def decode_packet(self, packet: str):  # return the sum of the version number of each sub-packet(self.version_nums)
        if self.is_operator_id_0(packet):
            self.deal_with_operator_id_0(packet)

        elif self.is_operator_id_1(packet):
            self.deal_with_operator_id_1(packet)

        else:
            self.version_nums += int(packet[self.next_str:self.next_str + 2 + 1], 2)

        while self.packet_list:
            print(self.packet_list)
            if self.is_operator_id_0(packet):
                self.deal_with_operator_id_0(packet)
            elif self.is_operator_id_1(packet):
                self.deal_with_operator_id_1(packet)
            else:
                self.deal_with_literal_values(packet)

    def main(self, input_file):
        with open(input_file) as f:
            for line in f.readlines():
                line = line.strip('\n')
                self.decode_packet(self.get_binary(line))


if __name__ == '__main__':
    sol = Solution()
    sol.main("input_task16.txt")
    print(sol.version_nums, "Final")
    #binary = sol.get_binary("C200B40A82")
    #print(binary, "Binary")
    #sol.decode_packet(binary)



