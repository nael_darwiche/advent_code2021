class AXIS:
    x = 0
    y = 1


class Task13Origami:
    def __init__(self):
        self.points = []
        self.instructions = []

    def read_file(self, filepath: str):
        reading_points = True
        with open(filepath, "r") as f:
            for line in f:
                line = line.strip(" \n")
                if reading_points:
                    if line:
                        self.points.append(self.parse_point(line))
                    else:
                        reading_points = False
                else:
                    self.instructions.append(self.parse_instruction(line))

    @staticmethod
    def parse_point(point: str) -> tuple[int, int]:
        x, y = point.split(',')
        return (int(x), int(y))

    @staticmethod
    def parse_instruction(instruction: str) -> tuple[int, int]:
        first, second = instruction.split('=')
        first = getattr(AXIS, first[-1])
        return first, int(second)

    def fold_along_x(self, index: int):
        newlist = []
        for x, y in self.points:
            if not x < index:
                continue
            x = 2 * index - x
            newlist.append((x, y))
        self.points = newlist


