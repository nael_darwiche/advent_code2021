seven_seg_display_dict = {('a', 'b', 'c', 'e', 'f', 'g'): 0, ('c', 'f'): 1, ('a', 'c', 'd', 'e', 'g'): 2,
                          ('a', 'c', 'd', 'f', 'g'): 3, ('b', 'c', 'd', 'f'): 4,
                          ('a', 'b', 'd', 'f', 'g'): 5, ('a', 'b', 'd', 'e', 'f', 'g'): 6, ('a', 'c', 'f'): 7,
                          ('a', 'b', 'c', 'd', 'e', 'f', 'g'): 8, ('a', 'b', 'c', 'd', 'f', 'g'): 9}


def get_all_possibilities(line: str) -> (dict, dict, dict, dict, dict, dict, dict, dict):
    signal_patterns = line.split('|')[0].strip(' \n').split(' ')
    for digit in signal_patterns:
        if len(digit) == 2:
            num_2 = digit
        elif len(digit) == 3:
            num_3 = digit
        elif len(digit) == 4:
            num_4 = digit
        elif len(digit) == 7:
            num_7 = digit
        # if len(digit) in ['2', '3', '4', '7']:
        # digit = int('num_'+str(len(digit)))
    ori_dict = {'a': num_3.replace(num_2[0], '').replace(num_2[1], ''),
                'c': (num_2[0], num_2[1]),
                'f': (num_2[0], num_2[1]),  # c and f are the same
                'b': (num_4.replace(num_2[0], '').replace(num_2[1], '')[0],
                      num_4.replace(num_2[0], '').replace(num_2[1], '')[1]),
                'd': (num_4.replace(num_2[0], '').replace(num_2[1], '')[0],
                      num_4.replace(num_2[0], '').replace(num_2[1], '')[1])
                }  # b and d are the same
    segment_e_g = ''
    for char in num_7:
        if char not in num_3 + num_4:
            segment_e_g += char
    ori_dict['e'] = (segment_e_g[0], segment_e_g[1])
    ori_dict['g'] = (segment_e_g[0], segment_e_g[1])

    dict1 = {'a': ori_dict['a'], 'b': ori_dict['b'][0], 'c': ori_dict['c'][0], 'd': ori_dict['d'][1],
             'e': ori_dict['e'][0], 'f': ori_dict['f'][1], 'g': ori_dict['g'][1]}
    dict2 = {'a': ori_dict['a'], 'b': ori_dict['b'][0], 'c': ori_dict['c'][0], 'd': ori_dict['d'][1],
             'e': ori_dict['e'][1], 'f': ori_dict['f'][1], 'g': ori_dict['g'][0]}
    dict3 = {'a': ori_dict['a'], 'b': ori_dict['b'][1], 'c': ori_dict['c'][0], 'd': ori_dict['d'][0],
             'e': ori_dict['e'][0], 'f': ori_dict['f'][1], 'g': ori_dict['g'][1]}
    dict4 = {'a': ori_dict['a'], 'b': ori_dict['b'][1], 'c': ori_dict['c'][0], 'd': ori_dict['d'][0],
             'e': ori_dict['e'][1], 'f': ori_dict['f'][1], 'g': ori_dict['g'][0]}
    dict5 = {'a': ori_dict['a'], 'b': ori_dict['b'][0], 'c': ori_dict['c'][1], 'd': ori_dict['d'][1],
             'e': ori_dict['e'][0], 'f': ori_dict['f'][0], 'g': ori_dict['g'][1]}
    dict6 = {'a': ori_dict['a'], 'b': ori_dict['b'][0], 'c': ori_dict['c'][1], 'd': ori_dict['d'][1],
             'e': ori_dict['e'][1], 'f': ori_dict['f'][0], 'g': ori_dict['g'][0]}
    dict7 = {'a': ori_dict['a'], 'b': ori_dict['b'][1], 'c': ori_dict['c'][1], 'd': ori_dict['d'][0],
             'e': ori_dict['e'][0], 'f': ori_dict['f'][0], 'g': ori_dict['g'][1]}
    dict8 = {'a': ori_dict['a'], 'b': ori_dict['b'][1], 'c': ori_dict['c'][1], 'd': ori_dict['d'][0],
             'e': ori_dict['e'][1], 'f': ori_dict['f'][0], 'g': ori_dict['g'][0]}
    return dict1, dict2, dict3, dict4, dict5, dict6, dict7, dict8


def check_if_works(dic: dict, line: str) -> bool:
    counter = 0
    signal_patterns = line.split('|')[0].strip(' \n').split(' ')
    for digit in signal_patterns:
        translated_digit = digit.translate(
            {ord(dic['a']): 'a', ord(dic['b']): 'b', ord(dic['c']): 'c', ord(dic['d']): 'd',
             ord(dic['e']): 'e', ord(dic['f']): 'f', ord(dic['g']): 'g'})
        for key in seven_seg_display_dict:
            if set(translated_digit) == set(key):
                counter += 1
                break
    return counter == 10


def calculate_output_value(line: str):
    list_output_values = []
    output_nums = line.split('|')[1].strip(' \n').split(' ')
    for dic in get_all_possibilities(line):
        if check_if_works(dic, line):
            final_dic = dic
            break
    for num in output_nums:
        translated_num = num.translate(
            {ord(final_dic['a']): 'a', ord(final_dic['b']): 'b', ord(final_dic['c']): 'c', ord(final_dic['d']): 'd',
             ord(final_dic['e']): 'e', ord(final_dic['f']): 'f', ord(final_dic['g']): 'g'})
        for key in seven_seg_display_dict:
            if set(translated_num) == set(key):
                list_output_values.append(seven_seg_display_dict[key])
                break
    return int(''.join(map(str, list_output_values)))


def main(input_file):
    with open(input_file) as f:
        return sum(calculate_output_value(line) for line in f.readlines())


if __name__ == '__main__':
    print(main('input_task08.txt'))



