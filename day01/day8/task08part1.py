def count_known_digits(line: str) -> int:
    counter = 0
    nums_to_check = line.split('|')[1].strip(' \n').split(' ')
    for num in nums_to_check:
        if len(num) in [2, 3, 4, 7]:
            counter += 1
    return counter


def main(input_file):
    with open(input_file) as f:
        return sum(count_known_digits(line) for line in f)


if __name__ == '__main__':
    print(main('input_task08.txt'))
