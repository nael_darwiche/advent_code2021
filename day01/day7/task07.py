def calculate_used_fuel(check_num: int, list_num: list) -> int:
    return sum(sum(range(abs(num - check_num)+1)) for num in map(int, list_num))


def main(input_file):
    with open(input_file) as f:
        list_num_arg = f.readlines()[0].split(',')
        max_num = max(map(int, list_num_arg))
        return min(calculate_used_fuel(check, list_num_arg) for check in range(max_num + 1))


if __name__ == '__main__':
    print(main('input_task07.txt'))
