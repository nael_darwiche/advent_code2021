def after_one_step(energy_levels_dict: dict) -> (int, dict):
    list_of_zeros = []
    for coordinate, energy in energy_levels_dict.items():
        if energy < 9:
            energy_levels_dict[coordinate] += 1
        else:
            energy_levels_dict[coordinate] = 0
            list_of_zeros.append(coordinate)
    #print(energy_levels_dict)
    counter = len(list_of_zeros)
    while list_of_zeros:
        x, y = list_of_zeros.pop()
        neighbors = [(x - 1, y), (x, y - 1), (x + 1, y - 1), (x - 1, y - 1),
                     (x + 1, y), (x - 1, y + 1), (x, y + 1), (x + 1, y + 1)]
        for x_to_check, y_to_check in neighbors:
            if not 0 <= x_to_check < 10:
                continue
            elif not 0 <= y_to_check < 10:
                continue
            elif energy_levels_dict[(x_to_check, y_to_check)] == 0:
                continue
            elif energy_levels_dict[(x_to_check, y_to_check)] < 9:
                energy_levels_dict[(x_to_check, y_to_check)] += 1
                #print((x_to_check, y_to_check), energy_levels_dict[(x_to_check, y_to_check)], (x, y))

            else:
                counter += 1
                energy_levels_dict[(x_to_check, y_to_check)] = 0
                list_of_zeros.append((x_to_check, y_to_check))
    return counter, energy_levels_dict


def main(input_file, steps: int):
    counter = 0
    dumbo_octopuses = {}
    with open(input_file) as f:
        f = f.readlines()
        for i, y in enumerate(f):
            y = y.strip('\n')
            for j, x in enumerate(y):
                dumbo_octopuses[(j, i)] = int(x)
    for i in range(steps):
        flashes, dumbo_octopuses = after_one_step(dumbo_octopuses)
        counter += flashes
        #print(dumbo_octopuses, dumbo_octopuses.values(), i)
        if dumbo_octopuses.values == [0] * 100:
            print(i)

        else:
            continue



#print(main('test.txt', 200))
print(main('input_task11.txt', 500))
