from collections import defaultdict
from queue import PriorityQueue


class Solution:
    def __init__(self):
        self.smallest_value_to_node = defaultdict(int)  # to our knowledge

        # LifoQueue = Stack
        # Queue = Queue
        self.nodes_to_explore = PriorityQueue()
        self.lines = []
        self.coordinates_of_the_full_map = {}

    def read_file_and_create_full_map(self, input_file):
        with open(input_file) as f:
            self.lines = f.readlines()
            for j, y in enumerate(self.lines):
                for i, x in enumerate(self.lines[j].strip('\n')):
                    for m in range(5):  # write(i, j) in all its new locations
                        for n in range(5):
                            if (int(x) + m + n) % 9 == 0:
                                self.coordinates_of_the_full_map[
                                    (i + n * len(self.lines[0].strip('\n')), j + m * len(self.lines))] = 9
                            else:
                                self.coordinates_of_the_full_map[
                                    (i + n * len(self.lines[0].strip('\n')), j + m * len(self.lines))] = (
                                                                                                                 int(x) + m + n) % 9
        return self.coordinates_of_the_full_map

    def execute_dijkstras_algorithm(self):
        self.smallest_value_to_node[(0, 0)] = 1  # to return True at line 62
        self.nodes_to_explore.put((self.smallest_value_to_node[(0, 0)] + len(self.lines[0].strip('\n')) * 5 - 1 + len(
            self.lines) * 5 - 1, (0, 0)))

        # FIFO -> First in first out (First come first serve)
        # []
        # [1,2,3]
        # for list: pop(0)  # for queues: get()
        # 1 [2, 3]
        # add 4
        # [2, 3, 4]
        # pop()
        # 4 [2, 3]

        while not self.nodes_to_explore.empty():
            node = self.nodes_to_explore.get()
            x, y = node[1]
            manhattan_dis_to_node = len(self.lines[0].strip('\n')) * 5 - 1 - x + len(self.lines) * 5 - 1 - y
            smallest_value_to_node = node[0] - manhattan_dis_to_node

            for neighbour_node in [(x - 1, y), (x + 1, y), (x, y + 1), (x, y - 1)]:
                a, b = neighbour_node[0], neighbour_node[1]
                if not 0 <= a <= len(self.lines[0].strip('\n')) * 5 - 1:
                    continue
                elif not 0 <= b <= len(self.lines) * 5 - 1:
                    continue
                elif self.smallest_value_to_node[neighbour_node] and \
                        smallest_value_to_node + self.coordinates_of_the_full_map[(a, b)] >= \
                        self.smallest_value_to_node[neighbour_node]:
                    continue
                else:
                    manhattan_dis = len(self.lines[0].strip('\n')) * 5 - 1 - a + len(self.lines) * 5 - 1 - b
                    self.smallest_value_to_node[neighbour_node] = smallest_value_to_node + \
                                                                  self.coordinates_of_the_full_map[(a, b)]
                    self.nodes_to_explore.put(
                        (self.smallest_value_to_node[neighbour_node] + manhattan_dis, neighbour_node))

        return self.smallest_value_to_node

    def main(self, input_file):
        self.read_file_and_create_full_map(input_file)
        self.execute_dijkstras_algorithm()
        return self.smallest_value_to_node[(len(self.lines) * 5 - 1, len(self.lines[0].strip('\n')) * 5 - 1)] - 1


if __name__ == '__main__':
    sol = Solution()
    print(sol.main('input_task15.txt'))
    # print(sol.main('input_task15.txt'))
    # print(sol.read_file_and_create_full_map('test_task15.txt'))
