from collections import defaultdict


class Solution:
    def __init__(self):
        self.smallest_value_to_node = defaultdict(int)  # to our knowledge
        self.nodes_to_explore = []
        self.lines = []

    def read_file(self, input_file):
        with open(input_file) as f:
            for line in f.readlines():
                self.lines.append(line.strip('\n'))

    def execute_dijkstras_algorithm(self):
        self.smallest_value_to_node[(0, 0)] = 0  # we don't count the first node
        self.nodes_to_explore.append(((0, 0), self.smallest_value_to_node[(0, 0)]))
        while self.nodes_to_explore:
            for i, node in enumerate(self.nodes_to_explore):
                del self.nodes_to_explore[i]
                x, y = node[0]
                for neighbour_node in [(x - 1, y), (x + 1, y), (x, y + 1), (x, y - 1)]:
                    a, b = neighbour_node[0], neighbour_node[1]
                    if not 0 <= neighbour_node[0] <= len(self.lines[0]) - 1:
                        continue
                    elif not 0 <= neighbour_node[1] <= len(self.lines) - 1:
                        continue
                    elif self.smallest_value_to_node[neighbour_node] and \
                            node[1] + int(self.lines[b][a]) >= self.smallest_value_to_node[neighbour_node]:
                        continue
                    else:
                        self.smallest_value_to_node[neighbour_node] = node[1] + int(self.lines[b][a])
                        self.nodes_to_explore.append(((a, b), self.smallest_value_to_node[neighbour_node]))
        return self.smallest_value_to_node

    def main(self, input_file):
        self.read_file(input_file)
        self.execute_dijkstras_algorithm()
        return self.smallest_value_to_node[(len(self.lines) - 1, len(self.lines[0]) - 1)]


if __name__ == '__main__':
    sol = Solution()
    print(sol.main('input_task15.txt'))
