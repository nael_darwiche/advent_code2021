

def main(input_file):
    with open(input_file) as f:
        for line in f:
            aim = 0
            hor_pos = 0
            depth = 0
            instr_word = line.split(' ')[0]
            instr_num = line.split(' ')[1]
            if instr_word == 'forward':
                hor_pos += int(instr_num)
                depth += aim*int(instr_num)
            elif instr_word == 'up':
                aim -= int(instr_num)
            elif instr_word == 'down':
                aim += int(instr_num)
    return hor_pos, depth


if __name__ == '__main__':
    print(main('input_task02.txt'))
