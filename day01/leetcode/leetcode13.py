class Solution:
    def romanToInt1(self, s: str) -> int:
        counter = 0
        romans_dict = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        gen_s = (dig for dig in s)
        last = next(gen_s)
        for i in gen_s:
            current = i
            if romans_dict[current] <= romans_dict[last]:
                counter += romans_dict[last]
            else:
                counter += (romans_dict[current] - romans_dict[last])
                try:
                    current = next(gen_s)
                except:
                    break
            last = current
        if romans_dict[s[len(s) - 1]] <= romans_dict[s[len(s) - 2]]:
            counter += romans_dict[s[len(s) - 1]]
        return counter

    def romanToInt2(self, s: str) -> int:
        NumeralMap = (('M', 1000),
                      ('CM', 900),
                      ('CD', 400),
                      ('D', 500),
                      ('C', 100),
                      ('XC', 90),
                      ('L', 50),
                      ('XL', 40),
                      ('X', 10),
                      ('IX', 9),
                      ('V', 5),
                      ('IV', 4),
                      ('I', 1))

        res = 0
        ind = 0

        for numeral, integer in NumeralMap:
            while s[ind:ind + len(numeral)] == numeral:
                res += integer
                ind += len(numeral)

        return res

    def romanToInt3(self, s: str) -> int:
        conversions = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000
        }

        solution = conversions[s[len(s) - 1]]
        for i in range(len(s) - 1):
            if conversions[s[i]] < conversions[s[i + 1]]:
                solution -= conversions[s[i]]
            else:
                solution += conversions[s[i]]

        return solution


sol = Solution()

assert 3 == sol.romanToInt1('III')
assert 4 == sol.romanToInt1('IV')
assert 14 == sol.romanToInt1('XIV')
assert 1994 == sol.romanToInt1('MCMXCIV')
assert 3, 999 == sol.romanToInt1('MMMCMXCIX')
assert 58 == sol.romanToInt2("LVIII")

print('Hello', sol.romanToInt3('III'))
