class Solution:
    def isPalindrome(self, x: int) -> bool:
        lis = list(str(x))
        if ''.join(lis) == ''.join(lis[::-1]):
            return True
        else:
            return False

    def isPalindrome2(self, x: int) -> bool:
        lis = list(str(x))
        return ''.join(lis) == ''.join(lis[::-1])

    def isPalindrome3(self, x: int) -> bool:
        return str(x) == str(x)[::-1]

    def isPalindrome4(self, x: int) -> bool:
        num = str(x)

        return num[:int(len(num)/2)] == num[::-1][:int(len(num)/2)]




sol = Solution()
assert True == sol.isPalindrome(121), '121 should be palindrome'
assert False == sol.isPalindrome(122), '122 should not be palindrome'
assert False == sol.isPalindrome(10), '10 should not be palindrome'
assert False == sol.isPalindrome(-121), '-121 should not be palindrome'
assert True == sol.isPalindrome4(12321), '123321 should be palindrome'

print('success')
