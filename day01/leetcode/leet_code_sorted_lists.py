list1 = [1, 2, 4]
list2 = [1, 3, 4]


class MergeTwoLists:
    def __init__(self):
        pass

    def merge_lists(self, lis1: list, lis2: list) -> list:
        final_lis = []
        idx1 = 0
        idx2 = 0

        for i in range(len(lis1)+len(lis2)):
            if idx1 >= len(lis1):
                final_lis.extend(lis2[idx2:])
                break
            elif idx2 >= len(lis2):
                final_lis.extend(lis1[idx1:])
                break
            elif lis1[idx1] >= lis2[idx2]:
                final_lis.append(lis2[idx2])
                idx2 += 1
            else:
                final_lis.append(lis1[idx1])
                idx1 += 1
        return final_lis


if __name__ == '__main__':
    list1 = [1, 2, 4]
    list2 = [10]
    final = [1, 1, 2, 3, 4, 4, 5, 6, 7, 8]
    merger = MergeTwoLists()
    print(merger.merge_lists(list1, list2))
