import array
from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def __str__(self):
        return f"ListNode({self.val}, {self.next and self.next.val})"

def print_all_linked_listnodes(start_node: ListNode):
    print(start_node.val)
    if start_node.next is not None:
        print_all_linked_listnodes(start_node.next)

def create_listnode_based_on_list(list_of_int):
    first_node = None
    last_node = None
    for num in list_of_int:
        new_listnode = ListNode(num, None)
        if first_node is None:
            first_node = new_listnode
        if last_node is not None:
            last_node.next = new_listnode
        last_node = new_listnode

    return first_node


class Solution:
    def merge(self, list1: Optional[ListNode], list2: Optional[ListNode]):
        list1.val
        list1.next


if __name__ == '__main__':
    sol = Solution()
    my_list_node1 = create_listnode_based_on_list([1, 3, 4])  # type: ListNode
    my_list_node2 = create_listnode_based_on_list([1, 2, 5])  # type: ListNode
    sol.merge(my_list_node1, my_list_node2)


    # turn list to linked ListNodes
    list_node_1 = ListNode(1, None)
    list_node_2 = ListNode(2, None)
    list_node_3 = ListNode(3, None)
    list_node_4 = ListNode(4, None)

    list_node_1.next = list_node_2
    list_node_2.next = list_node_3
    list_node_3.next = list_node_4
    #
    #
    # LinkedLIST vs ARRAY
    #
    # LINKEDLIST:
    # -> much work to read, easy to write
    # [1]["2"][3][4][5][6]
    #     [2.5]
    # [1]["2"][2.5][3][4][5][6]
    #
    #
    # ARRAY:
    # -> easy to read, much work to write / extend
    # 4bytes * 100 = 400bytes =>
    # [0][0][0][0][0][0][0][0][0][0]
    # [0][0][0][0][0][1][0][0][0][0][0]

    print_all_linked_listnodes(list_node_1)
