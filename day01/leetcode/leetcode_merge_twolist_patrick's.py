        res = ListNode()
        current = res
        while l1 and l1.val is not None:
            while l2 and l2.val is not None and l2.val<l1.val:
                current.next = l2
                l2 = l2.next
                current = current.next
            current.next = l1
            l1 = l1.next
            current = current.next
        current.next = l2
        res = res.next
        return res