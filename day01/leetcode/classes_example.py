class Human:
    count = 0  # this VLASS VARIABLE is available to all instances as the same value always

    def __init__(self, age):
        Human.count += 1  # this increases the CLASS VARIABLE, which is the same for all instances
        self.age = age  # this sets the INSTANCE VARIABLE, all instance have their own, independent version

    def get_older(self):
        self.age += 1

    def die(self):
        Human.count -= 1

    @classmethod
    def get_count(cls):
        """Class method refers to class variables only"""
        return cls.count

    @staticmethod
    def _is_birthday():
        """Static Methods cannot use instance variables or class variables"""
        return True

    # def __str__(self):
    #     return f"Human, age={self.age}"

    def __repr__(self):
        return f"Human(age={self.age})"


# we build a container class, which can hold many human
class Population:
    def __init__(self):
        self.humans: list[Human] = []

    def add_human(self, human: Human):
        self.humans.append(human)

    def show_humans(self):
        print("=" * 100)
        print("Current Human Population: ")
        for h in self.humans:
            print(h)
        print("=" * 100)


if __name__ == '__main__':
    Human.age = 100
    human1 = Human(10)
    human2 = Human(30)
    print("human1.age", human1.age)

    print("human1.get_older()", human1.get_older())
    print("human1.age", human1.age)
    print("human1", human1)
    print("human1.get_count", human1.get_count())
    print("Human.get_count", Human.get_count())

    print("human1.die", human1.die())
    print("Human.get_count", Human.get_count())
    print("human1._is_birthday", human1._is_birthday())

    pop = Population()
    pop.add_human(human1)
    pop.add_human(human2)
    pop.show_humans()

#     >>> id(human1.count) , id(human2.count)
#     (140703771015008, 140703771015008)
#     >>> id(human1.age) , id(human2.age)
#     (140703771015264, 140703771015904)