from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __str__(self):
        return f"ListNode({self.val}, {self.next and self.next.val})"


def create_listnode_based_on_list(list_of_int):
    first_node = None
    last_node = None
    for num in list_of_int:
        new_listnode = ListNode(num, None)
        if first_node is None:
            first_node = new_listnode
        if last_node is not None:
            last_node.next = new_listnode
        last_node = new_listnode

    return first_node


class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        return_linked_lis = ListNode()
        first_node = return_linked_lis
        if list1 is None:
            return list2
        elif list2 is None:
            return list1
        while True:
            if list1.val >= list2.val:
                return_linked_lis.next = ListNode(list2.val, next=None)
                return_linked_lis = return_linked_lis.next
                if list2.next is not None:
                    list2 = list2.next
                else:
                    return_linked_lis.next = list1
                    break
            else:
                return_linked_lis.next = ListNode(list1.val, next=None)
                return_linked_lis = return_linked_lis.next
                if list1.next is not None:
                    list1 = list1.next
                else:
                    return_linked_lis.next = list2
                    break
        return first_node.next


if __name__ == '__main__':
    sol = Solution()
    my_list_node1 = create_listnode_based_on_list([])  # type: ListNode
    my_list_node2 = create_listnode_based_on_list([1, 2, 5])  # type: ListNode
    print(sol.mergeTwoLists(my_list_node1, my_list_node2))
