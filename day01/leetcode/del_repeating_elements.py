class Solution:
    def removeDuplicates(self, nums: list[int]) -> int:
        del_idx = 0
        for idx in range(1, len(nums)):
            idx -= del_idx
            if nums[idx] == nums[idx-1]:
                del nums[idx-1]
                del_idx += 1
            else:
                pass
        return nums


if __name__ == '__main__':
    sol = Solution()
    print(sol.removeDuplicates([1, 1]))
