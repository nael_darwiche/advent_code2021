class Solution1:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        counter = 0
        for letter in zip(*strs):
            if ''.join(letter) == letter[0]*len(strs):
                counter += 1
            else:
                break
        return strs[0][:counter]



class Solution2:
    def longestCommonPrefix(self, words: List[str]) -> str:
        prefix = words[0]
        for word in words[1:]:
            # keep looping and current word sliced to len of prefix == prefix
            while word[:len(prefix)] != prefix:
                prefix = prefix[:-1]
                # Covers edge case: if two words don't have any similar characters
                if not prefix:
                    return ""
        return prefix


