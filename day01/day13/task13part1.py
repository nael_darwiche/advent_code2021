from collections import defaultdict


def fold_at_x(pos_of_dots: list[tuple], x: int) -> set[tuple]:
    for i, point in enumerate(pos_of_dots):
        if point[0] < x:
            pos_of_dots[i] = (2 * x - point[0], point[1])
        else:
            continue
    return set(pos_of_dots)


def fold_at_y(pos_of_dots: list[tuple], y: int) -> set[tuple]:
    for i, point in enumerate(pos_of_dots):
        if point[1] > y:
            pos_of_dots[i] = (point[0], 2 * y - point[1])
        else:
            continue
    return set(pos_of_dots)


def get_coordinates_after_all_folds(input_file):
    with open(input_file) as f:
        pos_of_dots = []
        for line in f.readlines():
            pos_of_dots.append((int(line.strip('\n').split(',')[0]), int(line.strip('\n').split(',')[1])))
        for x in [5]:        #[655, 327, 163, 81, 40]:
            pos_of_dots = fold_at_x(list(pos_of_dots), x)
        for y in [7]:        #[447, 223, 111, 55, 27, 13, 6]:
            pos_of_dots = fold_at_y(list(pos_of_dots), y)
        return pos_of_dots


def get_one_line_at_y_coord(list_of_points_at_y: list[tuple], y: int):
    lis = []
    for x in range(1500):
        if (x, y) in list_of_points_at_y:
            lis.insert(x, '#')
        else:
            lis.insert(x, '.')
    return ''.join(lis)


def main(input_file):
    dict_lines_at_y = defaultdict(list)
    pos_of_all_points = list(get_coordinates_after_all_folds(input_file))
    for y in range(10):
        for point in pos_of_all_points:
            if int(point[1]) == y:
                dict_lines_at_y[y].append(point)
            else:
                continue
    for y in range(10):
        print(get_one_line_at_y_coord(dict_lines_at_y[y], y))


if __name__ == '__main__':
    #print(get_coordinates_after_all_folds('input_task13.txt'))
    print(main('test.txt'))
