matching_char = {'(': ')', '{': '}', '[': ']', '<': '>'}
scoring_dict = {')': 1, ']': 2, '}': 3, '>': 4}


def find_closing_character(line: str) -> list[tuple]:
    closing_char_list = []
    for i, char in enumerate(line):
        if char in ['}', ']', ')', '>']:
            closing_char_list.append((char, i))
    return closing_char_list


def check_if_correct(line: str):  # return the illegal character if corrupted, else return True
    for tuple in find_closing_character(line):
        char = tuple[0]
        i = tuple[1]
        count_opening_and_closing_char = 1
        while count_opening_and_closing_char:
            if line[i - 1] in ['}', ']', ')', '>']:
                count_opening_and_closing_char += 1
                i -= 1
            else:
                i -= 1
                count_opening_and_closing_char -= 1
        if char != matching_char[line[i]]:
            return False
    return True


def find_sequence_closing_char(line: str):  # input is not a corrupted line, just an incomplete one.
    i = len(line)
    count_opening_and_closing_char = 1
    line += 'X'
    while count_opening_and_closing_char:
        if line[i-1] in ['}', ']', ')', '>']:
            count_opening_and_closing_char += 1
            i -= 1
            if i < 0:
                return False
        else:
            count_opening_and_closing_char -= 1
            i -= 1
            if i < 0:
                return False
    lis = list(line)
    lis[len(line)-1] = matching_char[line[i]]
    return ''.join(lis)


def main(input_file):
    with open(input_file) as f:
        list_of_scores = []
        print(len(list_of_scores))
        for line in (line for line in f if check_if_correct(line)):
            line = line.strip('\n')
            i = len(line)
            total_score = 0
            while find_sequence_closing_char(line) != False:
                line = find_sequence_closing_char(line)
            for char in line[i:]:
                total_score = total_score*5 + scoring_dict[char]
            list_of_scores.append(int(total_score))
            list_of_scores.sort()
        return list_of_scores[27]


if __name__ == '__main__':
    print(main('input_task10.txt'))








