matching_char = {'(': ')', '{': '}', '[': ']', '<': '>'}


def find_closing_character(line: str) -> list[tuple]:
    closing_char_list = []
    for i, char in enumerate(line):
        if char in ['}', ']', ')', '>']:
            closing_char_list.append((char, i))
    return closing_char_list


def check_if_corrupted(line: str):  # return the illegal character if corrupted, else return True
    for tuple in find_closing_character(line):
        char = tuple[0]
        i = tuple[1]
        print(i, find_closing_character(line))
        count_opening_and_closing_char = 1
        while count_opening_and_closing_char:
            if line[i - 1] in ['}', ']', ')', '>']:
                count_opening_and_closing_char += 1
                i -= 1
            else:
                i -= 1
                count_opening_and_closing_char -= 1
        print(line[i-1], line[i], line[i+1], line)
        if char != matching_char[line[i]]:
            return char
    return True


def main(input_file):
    counter = 0
    with open(input_file) as f:
        for line in f:
            if check_if_corrupted(line) == ')':
                counter += 3
            elif check_if_corrupted(line) == ']':
                counter += 57
            elif check_if_corrupted(line) == '}':
                counter += 1197
            elif check_if_corrupted(line) == '>':
                counter += 25137
            else:
                continue
    return counter


if __name__ == '__main__':
    print(main('input_task10.txt'))
